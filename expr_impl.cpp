#include "expr_impl.hpp"

#include <iostream>
#include <cmath>
#include <limits>
#include <utility>

namespace exprs {

    variable::variable(std::string name) : name(std::move(name)) {}

    void variable::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out<<name;
    }

    double variable::evaluate(const expr_base::variable_map_t &variables) const {
        for(auto const& var : variables){
            if(var.first==name){
                return var.second;
            }
        }
        throw unbound_variable_exception("variable "+name+" does not have assigned value");
    }

    expr variable::simplify() const {
        return shared_from_this();
    }

    expr variable::derive(std::string const &variable) const {
        return variable==name ? expr::ONE : expr::ZERO;
    }

    bool variable::equals(const expr_base &b) const {
        try {
            return dynamic_cast<const variable &>(b).name == name;
        }catch(...){
            return false;
        }
    }

//----------------------------------------------------------------------------------------------------------------------

    number::number(double value) : value(value){}

    void number::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out<<value;
    }

    double number::evaluate(const expr_base::variable_map_t &variables) const {
        return value;
    }

    expr number::simplify() const {
        return shared_from_this();
    }

    expr number::derive(std::string const &variable) const {
        return expr::ZERO;
    }

    bool number::equals(const expr_base &b) const {
        try{
            return dynamic_cast<const number&>(b).value==value;
        }catch(...){
            return false;
        }
    }

//----------------------------------------------------------------------------------------------------------------------

    operation::operation(std::string print) : print(std::move(print)) {}

//----------------------------------------------------------------------------------------------------------------------

    singleArgumentOperation::singleArgumentOperation(std::string print, expr argument) : operation(std::move(print)), argument(std::move(argument)) {}

    void singleArgumentOperation::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        switch(fmt){
            default:
                out<<"("<<print<<" "<<argument<<")";
                break;
        }
    }

    bool singleArgumentOperation::equals(const expr_base &b) const {
        try {
            return dynamic_cast<const singleArgumentOperation &>(b).print == print &&
                   dynamic_cast<const singleArgumentOperation &>(b).argument == argument;
        }catch(...){
            return false;
        }
    }

//----------------------------------------------------------------------------------------------------------------------

    sinOp::sinOp(expr argument) : singleArgumentOperation("sin",std::move(argument)) {}

    double sinOp::evaluate(const expr_base::variable_map_t &variables) const {
        return sin(argument->evaluate(variables));
    }

    expr sinOp::derive(std::string const &variable) const {
        return cos(argument)*argument->derive(variable);
    }

    expr sinOp::simplify() const {
        return sin(argument->simplify());
    }

//----------------------------------------------------------------------------------------------------------------------

    cosOp::cosOp(expr argument) : singleArgumentOperation("cos",std::move(argument)) {}

    double cosOp::evaluate(const expr_base::variable_map_t &variables) const {
        return cos(argument->evaluate(variables));
    }

    expr cosOp::derive(std::string const &variable) const {
        return (expr::ZERO-sin(argument))*argument->derive(variable);
    }

    expr cosOp::simplify() const {
        return cos(argument->simplify());
    }

//----------------------------------------------------------------------------------------------------------------------

    logOp::logOp(expr argument) : singleArgumentOperation("log",std::move(argument)) {}

    double logOp::evaluate(const expr_base::variable_map_t &variables) const {
        double tmp = argument->evaluate(variables);
        if(tmp<=0) throw domain_exception("Argument for log must be > 0");
        return log(tmp);
    }

    expr logOp::derive(std::string const &variable) const {
        return argument->derive(variable)/argument;
    }

    expr logOp::simplify() const {
        expr tmp = argument->simplify();
        return tmp==expr::ONE ? expr::ZERO : log(tmp);
    }

//----------------------------------------------------------------------------------------------------------------------

    binaryArgumentOperation::binaryArgumentOperation(std::string print, expr lhs, expr rhs) : operation(std::move(print)), lhs(std::move(lhs)), rhs(std::move(rhs)) {}

    void binaryArgumentOperation::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        switch(fmt){
            default:
                out<<"("<<print<<" "<<lhs<<" "<<rhs<<")";
                break;
        }
    }

    bool binaryArgumentOperation::equals(const expr_base &b) const {
        try {
            return dynamic_cast<const binaryArgumentOperation &>(b).print == print &&
                   dynamic_cast<const binaryArgumentOperation &>(b).lhs == lhs &&
                   dynamic_cast<const binaryArgumentOperation &>(b).rhs == rhs;
        }catch(...){
            return false;
        }
    }

//----------------------------------------------------------------------------------------------------------------------

    plus::plus(expr lhs, expr rhs) : binaryArgumentOperation("+", std::move(lhs), std::move(rhs)) {}

    double plus::evaluate(const expr_base::variable_map_t &variables) const {
        return lhs->evaluate(variables)+rhs->evaluate(variables);
    }

    expr plus::derive(std::string const &variable) const {
        return lhs->derive(variable)+rhs->derive(variable);
    }

    expr plus::simplify() const {
        expr tmp1 = lhs->simplify();
        expr tmp2 = rhs->simplify();
        return tmp1==expr::ZERO ? tmp2 : (tmp2==expr::ZERO ? tmp1 : tmp1+tmp2);
    }

//----------------------------------------------------------------------------------------------------------------------

    minus::minus(expr lhs, expr rhs) : binaryArgumentOperation("-", std::move(lhs), std::move(rhs)) {}

    double minus::evaluate(const expr_base::variable_map_t &variables) const {
        return lhs->evaluate(variables)-rhs->evaluate(variables);
    }

    expr minus::derive(std::string const &variable) const {
        return lhs->derive(variable)-rhs->derive(variable);
    }

    expr minus::simplify() const {
        expr tmp1 = lhs->simplify();
        expr tmp2 = rhs->simplify();
        return tmp2==expr::ZERO ? tmp1 : tmp1-tmp2;
    }

//----------------------------------------------------------------------------------------------------------------------

    multiply::multiply(expr lhs, expr rhs) : binaryArgumentOperation("*", std::move(lhs), std::move(rhs)) {}

    double multiply::evaluate(const expr_base::variable_map_t &variables) const {
        return lhs->evaluate(variables)*rhs->evaluate(variables);
    }

    expr multiply::derive(std::string const &variable) const {
        return (lhs->derive(variable)*rhs)+(lhs*rhs->derive(variable));
    }

    expr multiply::simplify() const {
        expr tmp1 = lhs->simplify();
        expr tmp2 = rhs->simplify();
        return tmp1==expr::ZERO||tmp2==expr::ZERO ? expr::ZERO : (tmp1==expr::ONE ? tmp2 : (tmp2==expr::ONE ? tmp1 : (tmp1*tmp2)));
    }

//----------------------------------------------------------------------------------------------------------------------

    divide::divide(expr lhs, expr rhs) : binaryArgumentOperation("/", std::move(lhs), std::move(rhs)) {}

    double divide::evaluate(const expr_base::variable_map_t &variables) const {
        return lhs->evaluate(variables)/rhs->evaluate(variables);
    }

    expr divide::derive(std::string const &variable) const {
        return ((lhs->derive(variable)*rhs)-(lhs*rhs->derive(variable)))/pow(rhs,expr::number(2));
    }

    expr divide::simplify() const {
        expr tmp1 = lhs->simplify();
        expr tmp2 = rhs->simplify();
        return tmp1==expr::ZERO ? (tmp2==expr::ZERO ? expr::ZERO/expr::ZERO : expr::ZERO) : (tmp2==expr::ONE ? tmp1 : tmp1/tmp2);
    }

//----------------------------------------------------------------------------------------------------------------------

    powerOp::powerOp(expr lhs, expr rhs) : binaryArgumentOperation("^", std::move(lhs), std::move(rhs)) {}

    double powerOp::evaluate(const expr_base::variable_map_t &variables) const {
        return pow(lhs->evaluate(variables),rhs->evaluate(variables));
    }

    expr powerOp::derive(std::string const &variable) const {
        return pow(lhs,rhs)*((lhs->derive(variable)*rhs)/lhs+log(lhs)*rhs->derive(variable));
    }

    expr powerOp::simplify() const {
        expr tmp1 = lhs->simplify();
        expr tmp2 = rhs->simplify();
        return tmp2==expr::ZERO ? expr::ONE : (tmp1==expr::ZERO ? expr::ZERO : (tmp2==expr::ONE ? tmp1 : pow(tmp1,tmp2)));
    }

} // namespace exprs
