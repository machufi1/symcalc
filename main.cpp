#include <iostream>
#include "cmdline.hpp" // parse_command
#include "app.hpp" // handle_expr_line

int main(int argc, char *argv[]){
    std::vector<Commands::Command> commands;
    for(int i=1;i<argc;i++){
        commands.push_back(parse_command(argv[i]));
    }

    std::string input;
    while(getline(std::cin,input)){
        try{
            handle_expr_line(std::cout,input,commands);
        }catch(...){
            std::cout<<"exception caught hehe"<<std::endl;
        }
    }
}
