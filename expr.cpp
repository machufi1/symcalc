#include "expr.hpp"
#include "expr_impl.hpp"
#include "tokenizer.hpp"
#include <stdexcept>
#include <utility>
#include <sstream>
#include <vector>

const expr expr::ZERO = expr::number(0.0);
const expr expr::ONE = expr::number(1.0);

// TODO: overloaded operators +, -, *, /, functions pow, log, sin, cos,
//       expr::number, expr::variable, operator==, operator<<,
//       create_expression_tree

void addOperator(std::vector<expr>& stack, const Token& t){
    if(t.id==TokenId::Identifier){ //function
        if(stack.empty()) throw parse_error("Empty stack when at least 1 expression was expected");
        expr tmp = stack.back();
        stack.pop_back();
        if(t.identifier=="sin"){
            stack.push_back(sin(tmp));
        }else if(t.identifier=="cos"){
            stack.push_back(cos(tmp));
        }else if(t.identifier=="log"){
            stack.push_back(log(tmp));
        }else{
            throw parse_error("Function expected");
        }
    }else{ //binary operator
        if(!t.is_binary_op()) throw parse_error("Binary operator expected");
        if(stack.size()<2) throw parse_error("Stack contains less than 2 expressions, when at least 2 were expected");
        expr rhs = stack.back();
        stack.pop_back();
        expr lhs = stack.back();
        stack.pop_back();
        switch(t.id){
            case TokenId::Plus:
                stack.push_back(lhs+rhs);
                break;
            case TokenId::Minus:
                stack.push_back(lhs-rhs);
                break;
            case TokenId::Multiply:
                stack.push_back(lhs*rhs);
                break;
            case TokenId::Divide:
                stack.push_back(lhs/rhs);
                break;
            case TokenId::Power:
                stack.push_back(pow(lhs,rhs));
                break;
        }
    }
}

expr create_expression_tree(const std::string& expression) {
    std::istringstream ss = std::istringstream(expression);
    Tokenizer tokenizer(ss); //cpp wouldn't let me inline for unknown reason
    Token t=tokenizer.next();
    std::vector<expr> stack;
    std::vector<Token> operatorStack;
    bool expectingOperator = false;
    bool expectingLeftP = false;
    while(t.id!=TokenId::End){
        if(expectingLeftP){
            expectingLeftP=false;
            if(t.id!=TokenId::LParen){
                stack.push_back(expr::variable(operatorStack.back().identifier));
                operatorStack.pop_back();
                expectingOperator=true;
            }
        }
        if(expectingOperator){
            expectingOperator=false;
            if(t.id==TokenId::LParen) {
                throw unknown_function_exception("Ha");
            }else if(t.id==TokenId::Number||t.id==TokenId::Identifier){
                throw parse_error("Hi");
            }
        }
        switch(t.id){
            case TokenId::Number:
                stack.push_back(expr::number(t.number));
                expectingOperator=true;
                break;
            case TokenId::Identifier:
                if(t.identifier=="sin"||t.identifier=="cos"||t.identifier=="log"){
                    operatorStack.push_back(t);
                    expectingLeftP=true;
                }else{
                    stack.push_back(expr::variable(t.identifier));
                    expectingOperator=true;
                }
                break;
            case TokenId::Plus:
            case TokenId::Minus:
            case TokenId::Multiply:
            case TokenId::Divide:
            case TokenId::Power:
                if(!operatorStack.empty()) {
                    Token top = operatorStack.back();
                    while (top.id!=TokenId::LParen&&(top.id == TokenId::Identifier||top.op_precedence() > t.op_precedence()||(top.op_precedence()==t.op_precedence()&&top.associativity()==Associativity::Left))){
                        addOperator(stack,top);
                        operatorStack.pop_back();
                        if(operatorStack.empty()) break;
                        top=operatorStack.back();
                    }
                }
                operatorStack.push_back(t);
                break;
            case TokenId::LParen:
                operatorStack.push_back(t);
                break;
            case TokenId::RParen:
                if(operatorStack.empty()) throw parse_error("Mismatched parentheses");
                Token top = operatorStack.back();
                while(top.id!=TokenId::LParen){
                    addOperator(stack,top);
                    operatorStack.pop_back();
                    if(operatorStack.empty()) throw parse_error("Mismatched parentheses");
                    top=operatorStack.back();
                }
                operatorStack.pop_back();
                break;
        }
        t=tokenizer.next();
    }
    if(!operatorStack.empty()) {
        Token top = operatorStack.back();
        if(top.id==TokenId::Identifier&&expectingLeftP){
            stack.push_back(expr::variable(top.identifier));
            operatorStack.pop_back();
        }
        while (!operatorStack.empty()) {
            Token top = operatorStack.back();
            if (top.id == TokenId::LParen) throw parse_error("Mismatched parentheses");
            addOperator(stack, top);
            operatorStack.pop_back();
        }
    }
    if(stack.empty()) throw parse_error("Empty expression");
    if(stack.size()>1) throw parse_error("Expected only 1 expression to be left");
    return stack.back();
}

bool operator==(const expr &a, const expr &b) {
    return a->equals(*b.ptr.get());
}

std::ostream& operator<<(std::ostream &os, const expr &e) {
    e->write(os,expr_base::WriteFormat::Prefix);
    return os;
}

expr expr::number(double n) {
    return std::make_shared<exprs::number>(n);
}

expr expr::variable(std::string name){
    return std::make_shared<exprs::variable>(std::move(name));
}

expr operator+(expr a, expr b){
    return std::make_shared<exprs::plus>(std::move(a),std::move(b));
}

expr operator-(expr a, expr b){
    return std::make_shared<exprs::minus>(std::move(a),std::move(b));
}

expr operator*(expr a, expr b){
    return std::make_shared<exprs::multiply>(std::move(a),std::move(b));
}

expr operator/(expr a, expr b){
    return std::make_shared<exprs::divide>(std::move(a),std::move(b));
}

expr sin(expr e){
    return std::make_shared<exprs::sinOp>(std::move(e));
}

expr cos(expr e){
    return std::make_shared<exprs::cosOp>(std::move(e));
}

expr pow(expr m, expr e){
    return std::make_shared<exprs::powerOp>(std::move(m), std::move(e));
}

expr log(expr e){
    return std::make_shared<exprs::logOp>(std::move(e));
}
