#pragma once

#include "expr.hpp"
#include <iosfwd>

namespace exprs {
    class number : public expr_base{
    private:
        double value;
    public:
        explicit number(double value);

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;
    private:
        void write(std::ostream& out, WriteFormat fmt) const override;

        bool equals(const expr_base& b) const override;
    };

    class variable : public expr_base{
    private:
        std::string name;
    public:
        explicit variable(std::string name);

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;
    private:
        void write(std::ostream& out, WriteFormat fmt) const override;

        bool equals(const expr_base& b) const override;
    };

    class operation : public expr_base{
    protected:
        std::string print;
    public:
        explicit operation(std::string print);
    };

    class singleArgumentOperation : public operation{
    protected:
        expr argument;
    private:
        void write(std::ostream& out, WriteFormat fmt) const override;
    public:
        singleArgumentOperation(std::string print, expr argument);

        bool equals(const expr_base& b) const override;
    };

    class sinOp : public singleArgumentOperation{
    public:
        explicit sinOp(expr argument);

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;
    };

    class cosOp : public singleArgumentOperation{
    public:
        explicit cosOp(expr argument);

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;
    };

    class logOp : public singleArgumentOperation{
    public:
        explicit logOp(expr argument);

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;
    };

    class binaryArgumentOperation : public operation{
    protected:
        expr lhs;
        expr rhs;
    private:
        void write(std::ostream& out, WriteFormat fmt) const override;
    public:
        binaryArgumentOperation(std::string print, expr  lhs, expr  rhs);

        bool equals(const expr_base& b) const override;
    };

    class plus : public binaryArgumentOperation{
    public:
        plus(expr lhs, expr rhs);

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;
    };

    class minus : public binaryArgumentOperation{
    public:
        minus(expr lhs, expr rhs);

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;
    };

    class multiply : public binaryArgumentOperation{
    public:
        multiply(expr lhs, expr rhs);

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;
    };

    class divide : public binaryArgumentOperation{
    public:
        divide(expr lhs, expr rhs);

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;
    };

    class powerOp : public binaryArgumentOperation{
    public:
        powerOp(expr lhs, expr rhs);

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;
    };
}
